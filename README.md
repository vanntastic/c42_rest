# c42REST 

a node module to interface with the Code 42 API

## Getting Started

```
npm install git+ssh://git@bitbucket.org/vanntastic/c42_rest.git
```

## Documentation

c42REST is the main class to interact with the c42 API.

c42REST has the following options for initialization:

- username: the username for your account
- password: the password for your account
- hostname: the hostname for your shareplan server
- port: the port for your shareplan server (defaults to 7280)
- baseUrl: the base url for the api entry point, you shouldn't need to change this
- protocol: the protocol that the server will be running on, it's usually http or https

### Methods

The REST based methods mentioned below are applicable to any resource in the apidocviewer.

#### authorize

Authorizes your account with the api and allows you to make api requests, this method should
be executed before making any requests.

#### get(resource, params, name)

Make a get request to a resource

##### parameters

- resource: name of the resource to access
- params: parameters that you want to pass to the resource
	- success: the success callback to run after a response
	- *: any other params that you want to pass to the resource
- name: a name for the response in the cache

#### post(resource, params, name)

Make a post request to a resource

##### parameters

- resource: name of the resource to access
- params: parameters that you want to pass to the resource
	- success: the success callback to run after a response
	- *: any other params that you want to pass to the resource
- name: a name for the response in the cache


#### put(resource, params, name)

Make a put request to a resource

##### parameters

- resource: name of the resource to access
- params: parameters that you want to pass to the resource
	- success: the success callback to run after a response
	- *: any other params that you want to pass to the resource

### uploadFiles(params, files) 

Upload files to shareplan

#### parameters

- params: 
	- planUid: the planUid of the plan
	- path: the path that you want to upload all the files to
	- succes: the success callback for completion of uploads

### curlFile(params)

Upload a file via curl, it's mainly a fallback if you are using c42REST from the 
OS.

#### parameters

- params: parameters to pass to the file resource
	- planUid: the planUid for the plan
	- path: the path in the plan where you want to upload to 
	- file: the file on the filesystem that you want to upload

## Examples

After you install rest42, simply require it:

```
var api = require('c42_rest');
var myApi = new api({
	username: 'myuser',
	password: 'mypassword'
});
myApi.authorize();
// now you can do things like:

myApi.get('user', { 
	success: function(response) {
		// do something with response		
	}
});
myApi.get('plan', {
	planUid: 123123123,
	success: function(response) {
		// do something with the response/plan	
	}
});

// create a new plan
myApi.post('Plan', {
    "name": "my new plan",
    "orgId": 2,
    "users": [{
        "userUid": "e0f6efaf8bd7edb1",
        "roleName": "Plan_Admin"
    }]
});

// upload a file via curl
myApi.curlFile({
	planUid: 123123123,
	path: '/path/to/folder/on/plan',
	file: '/file/on/local/file/system',
});
```

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_

## License
Unless otherwise noted, all files and works contained in the Code 42 projects are covered by the Apache 2 license as detailed below:

Copyright 2014, Code 42 Software Inc.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
