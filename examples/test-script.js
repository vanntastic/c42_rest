// EXAMPLES of how to use the c42-rest tool
// 
// init your rest class
var fs = require('fs')
var rest = require('./lib/c42_rest')
var sp = new rest({
    username: 'ab',
    password: 'asdfasdf'
});
sp.authorize();
var admin = new rest({
    username: 'admin',
    password: 'admin'
});
admin.authorize();
// example of ssl connection
// if it's a non signed ssl cert, make sure to set this as well : process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
var vann = new rest({
    username: 'foo@barbaz',
    password: 'foobarbaz',
    hostname: 'foo.baz.com',
    protocol: 'https',
    port: 4285
});

// NOTE: planUid MUST always be a string

// Example of how to upload a file
// need to fix this, it seems to only uplaod each of them as 0 byte files
// this isn't working 100%

// Upload multiple files
f = ':/' + path.join(process.cwd(), 'lib/c42_rest.js');

// x = fs.createReadStream('/Users/vek/Desktop/Desktop-foobar.jpg', {encoding: 'utf8'});
x = '/Users/vek/Desktop/Desktop-foobaz.jpg'
sp.uploadFiles({
    planUid: '621944318395559234',
    path: '',
    name: 'Desktop-foobar.jpg'
}, [x]);

// curl file example
sp.curlFile({
    planUid: '621944318395559234',
    path: '',
    file: '/Users/vek/Desktop/foobarbaz.txt'
})

// Create a plan
sp.post('Plan', {
    "name": "node plan",
    "orgId": 2,
    "users": [{
        "userUid": "e0f6efaf8bd7edb1",
        "roleName": "Plan_Admin"
    }]
})

// notice how the first one is NOT case sensitive and the last one is, some servers require case-sensitivity
// add a user to a plan
sp.post('planuserinvitation', {
    "roleName": "Plan_Contributor",
    "planUids": ["622147848953659658"],
    "userUids": ["999ce49d92e70142"]
})
vann.post('plan', {
    name: 'created from node again',
    orgId: 3,
    users: [98]
})
vann.post('PlanUserInvitation', {
    "planUids": ["622216876016813830"],
    "userUids": ["d9389e99e54ee340"]
})

sp.getFile({
    planUid: '621944318395559234',
    path: 'Desktop.jpg'
});
// Get a Plan
sp.get('Plan', {
    planUid: '621944318395559234'
})
// Get a Plan and save it
sp.get('Plan', {
    planUid: '621944318395559234'
}, 'myPlan');
// access the results using : sp.responses.myPlan

// Get a list of users in a plan
sp.get('planusers', {
    planUid: '621944318395559234'
})

// Get a User
sp.get('user', {
    userId: '2'
}, 'bob')

// Get your logged in information and save it
sp.get('user', {
    userId: 'my'
}, 'me')

var unirest = require('unirest');
var file = fs.createWriteStream('/Users/vek/Desktop/foo.mdown');
// request the file from a remote server
var rem = unirest.request('http://ab:asdfasdf@private:7280/api/file/621944318395559234/angular-presentation.mdown');
rem.on('data', function(chunk) {
    // instead of loading the file into memory
    // after the download, we can just pipe
    // the data as it's being downloaded
    file.write(chunk);
});
http://private:7280/api/file/621944318395559234/Desktop-2.jpg?t=0s6imywi20zql1cao7pnspjx0i