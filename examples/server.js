// Example of wiring up the tool to a server

var express = require('express');
var app = express();
var fs = require('fs')
var rest = require('../lib/c42_rest')
var request = require('request');
var sp;

var urlJoin = function(urlAry) {
	return urlAry.join("/");
};

// see: http://stackoverflow.com/questions/7067966/how-to-allow-cors-in-express-nodejs
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
app.use(allowCrossDomain);
app.use(express.bodyParser());

app.post('/api/auth', function(req, res) {
	sp = new rest({
		username: req.body.username,
		password: req.body.password
	});
	console.log('Authenticating::', req.body.username, req.body.password);
	sp.authorize(function(body){
		console.log("Authorization Succesful", body);
		res.send(body);
	});
});

app.get('/api/user', function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	sp.get('user', {
		success: function(body) {
			res.end(JSON.stringify(body));
		}
	});
});

app.get('/api/plan/:planUid', function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	sp.get('plan', {
		planUid: req.params.planUid,
		success: function(body) {
			res.end(JSON.stringify(body));
		}
	});	
});

// download a file
app.get('/api/file/:planUid/:path', function(req, res) {
	var file = sp.getFile({
		planUid: req.params.planUid,
		path: req.params.path,
	});
	request(file).pipe(res);
});

// this might work for uploading files, you should give it a shot!
app.post('/api/files', function(req, res) {
	// // CONTINUE HERE: attempt multi-file uploads
	console.log('UPLOADING FILES::', req.files.file);
	file = fs.createReadStream(req.files.file.path);
	sp.uploadFiles({
		planUid: req.body.planUid,
		path: req.body.path,
		name: req.files.file.originalFilename,
		success: function(resp) {
			console.log('UPLOAD successful');
			res.end();
		}
	}, [file]);
});

app.listen(3000);
console.log('Listening on port 3000');
