/*
 * Node interface to c42 rest API 
 * 
 * @TODO:
 *
 * 	- consider integrating a promise based model
 * 	- write tests!
 * 	- ensure we trap errors
 * 	- add a debug mode that allows you to see the requests output to console.log
 * 	- add failure callbacks
 * 	
 * Copyright (c) 2014 Code 42   
 */

var url = require('url');
var path = require('path');
var http = require('http');
var unirest = require('unirest');
var __ = require('underscore');
var mime = require('mime');
var Q = require('q');

// http://nodejs.org/api.html#_child_processes
var sys = require('sys')
var exec = require('child_process').exec;
var child;

var CMD = function(cmd, cb) {
	exec(cmd, function(error, stdout, stderr) {
		return cb(error,stdout,stderr)
	});
};

'use strict';

/**
 * c42REST class
 * 
 * @Usage:
 *
 * rest = require('c42REST')
 * sp = new rest({
 * 	username: 'me',
 * 	password: 'mypassword',
 * 	hostname: 'myshareplan.com',
 * 	port: 4280,
 * 	baseUrl: 'api', // shouldn't need to change this
 * 	protocol: 'http', // can be http or https
 * })	 
 * sp.authorize(); 
 *  
 */

var c42REST = function(opts) {
	var instance = this;
	instance.username = opts.username;
	instance.password = opts.password;
	instance.hostname = opts.hostname || 'private';
	instance.port = opts.port || 7280;
	instance.baseUrl = opts.baseUrl || 'api';
	instance.protocol = opts.protocol || 'http';
	instance.entryPoint = url.format({
		protocol: instance.protocol,
		port: instance.port,
		hostname: instance.hostname,
		pathname: instance.baseUrl
	});
	// TODO: add option to change the app code
	instance.APP_CODES = {
		consumer: 'CPW',
		business: 'CPO',
		enterprise: 'CPP'
	};

	// In memory response cache
	instance.responses = [];

	var base64 = {
		encode: function(str) {
			return new Buffer(str).toString('base64');
		},
		decode: function(str) {
			return new Buffer(str, 'base64').toString('ascii');
		}
	};

	/**
	 * authorize against the api
	 * @param  {function} success : a success callback
	 * @return {promise}         
	 */
	instance.authorize = function(success) {
		var dfr = Q.defer();
		var url = instance.entryPoint + '/AuthToken';
		var authHeader = "BASIC " + base64.encode(instance.username + ":" + instance.password);
		var headers = {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			'Authorization-Challenge': false,
			'Authorization': authHeader
		}
		var data = JSON.stringify({
			appCode: instance.APP_CODES.enterprise
		});
		unirest.post(url)
			.headers(headers)
			.send(data)
			.end(function(response) {
				if (response.body && response.body.data) {
					dfr.resolve(response.body);
					console.log("TOKEN RECEIVED::", response);
					instance.sessionToken = response.body.data[0],
					instance.urlToken = response.body.data[1],
					instance.token = instance.sessionToken + '-' + instance.urlToken;
				} else {
					dfr.reject(response);
				}
				if (success) {
					success(response.body);
				};
			});
		// return instance;
		return dfr.promise;
	};

	// TODO: need to add a way to check the authToken

	// all default auth headers will go here...
	instance._defaultHeaders = function() {
		return {
			"Authorization": "TOKEN "+instance.token
		};
	};

	/**
	 * append the id to the url for given resources 
	 * @param {object} params   the params object
	 * @param {string} resource the name of the resource
	 */
	instance._setIdOnUrl = function(resource, params, url) {
		resource = resource.toLowerCase();
		var isUserIdUrl = resource == 'user' && params.userId;
		var isPlanIdUrl = resource == 'plan' && params.planUid;
		var isFileIdUrl = resource == 'file' && params.planUid;
		if (isUserIdUrl) {
			return url + '/' + params.userId;
			delete params.userId;
		}
		if (isPlanIdUrl) {
			return url + '/' + params.planUid;
			delete params.planUid;
		}
		if (isFileIdUrl) {
			return url + '/' + params.planUid;
			delete params.planUid;
		}
		return url;
	};

	// for testing only
	instance.unirest = function(type, resource) {
		var url = instance.entryPoint + '/' + resource;
		var headers = __.extend({
			'Content-Type': 'application/json;charset=UTF-8',
			'Accept': 'application/json, text/plain, */*'
		}, instance._defaultHeaders());
		return unirest[type](url).headers(headers);
	};

	/**
	 * send a get request
	 * @param  {string} resource : name of the resource
	 * @param  {object} params   : params to send to the request
	 * @param  {string} name     : name of request in cache
	 * @return {promise}          
	 */
	instance.get = function(resource, params, name) {
		var dfr = Q.defer();
		params = params || {};
		var url = instance.entryPoint + "/" + resource;
		url = instance._setIdOnUrl(resource, params, url);
		var headers = __.extend({
			'Accept': 'application/json, text/plain, */*'
		}, instance._defaultHeaders());
		var success = params.success ? params.success : undefined;
		delete params.success;
		unirest.get(url)
			.headers(headers)
			.end(function(response) {
				console.log("GET request:", response)
				if (name) {
					instance.responses[name] = response.body;
				}
				if (response.body) {
					dfr.resolve(response.body);
				} else {
					dfr.reject(response);
				}
				if (success) {
					success(response.body);
				}
			});
		return dfr.promise;
	};

	/**
	 * returns a request that allows you to pipe to a stream
	 * @param  {object} params : params to pass to the reuqest
	 * @return {object}        returns a unirest object
	 */
	instance.getFile = function(params) {
		var url = instance.entryPoint + '/File/' + params.planUid + '/' + params.path + '?t=' + instance.urlToken;
		return unirest.request({url: url, headers: instance._defaultHeaders()});
	};

	/**
	 * send a put request
	 * @param  {string} resource : name of resource
	 * @param  {object} params   : params for the request
	 * @return {promise}          
	 */
	instance.put = function(resource, params) {
		var dfr = Q.defer();
		params = params || {};
		var url = instance.entryPoint + "/" + resource;
		unirest.put(url)
			.headers(instance._defaultHeaders())
			.send(params)
			.end(function(response) {
				console.log("PUT request", response);
				if (response.body) {
					dfr.resolve(response.body);
				} else {
					dfr.reject(response);
				}
			});
		return dfr.promise;
	};

	/**
	 * send a post request
	 * @param  {string} resource : name of the resource to post to
	 * @param  {object} params   : parameters for request
	 * @param  {string} name     : name of the request to put in the cache
	 * @return {promise}          
	 */
	instance.post = function(resource, params, name) {
		var dfr = Q.defer();
		params = params || {};
		var url = instance.entryPoint + "/" + resource;
		var headers = __.extend({
					'Content-Type': 'application/json;charset=UTF-8',
					'Accept': 'application/json, text/plain, */*'
				}, instance._defaultHeaders());
		unirest.post(url)
			.headers(headers)
			.send(JSON.stringify(params))
			.end(function(response) {
				console.log("POST request", response);
				if (name) {
					instance.responses[name] = response.body;
				}
				if (response.body) {
					dfr.resolve(response.body);
				} else {
					dfr.reject(response);
				}
			});
		return dfr.promise;
	};

	/**
	 * curlFile : a fallback for uploading files
	 * @param  {object} params : params to pass to the file api
	 */
	instance.curlFile = function(params) {
		var dfr = Q.defer();
		var planUid = params.planUid, path = params.path, file = params.file;
		var url = instance.entryPoint + "/File";
		var authHeader = "Authorization TOKEN "+instance.token;
		var mimeType = "type="+mime.lookup(file);
		var cmd = "curl -vv --form-string planUid="+planUid+" --form-string path="+path+" -F 'file=@"+file+";"+mimeType+"' \
								-u "+instance.username+":"+instance.password+" "+url;
		console.log("Exec::", cmd);
		CMD(cmd, function(error, stdout, stderr) {
			if (!error) {
				console.log("File uploaded successfully!", stdout);
				dfr.resolve(stdout);
			} else {
				console.log("Error occurred uploading file!", error);	
				dfr.reject(error);
			};
		});
		return dfr.promise;
	};

	/**
	 * upload files to the server : this currently doesn't work it seems like
	 * the server doesn't accept streamed files?
	 * @param  {object} params : list of params to send
	 * @param  {array} files  : an array of Stream-able file objects
	 * @return {promise}        
	 */
	instance.uploadFiles = function(params, files) {
		var dfr = Q.defer();
		var url = instance.entryPoint + "/File";
		url = params.name ? (url + '?file=' + params.name) : url;
		var success = params.success ? params.success : undefined;
		console.log('Calling upload files', params, files)
		for (var i = files.length - 1; i >= 0; i--) {
			var file = files[i];
			console.log('FILE TO UPLOAD', file)	
			return unirest.post(url)
				.headers(instance._defaultHeaders())
				.field('planUid', params.planUid)
				.field('path', params.path)
				.attach('file', file)
				.end(function(response) {
					console.log('File Uploaded', response);
					if (success) {
						success(response);
					}
				});

		};
	};
	
	/**
	 * DELETE request
	 * @param  {string} resource : name of resource
	 * @param  {object} params   : parameters to pass to the resource
	 */
	instance.delete = function(resource, params) {
		params = params || {};
		var url = instance.entryPoint + "/" + resource;
		unirest.delete(url)
			.headers(instance._defaultHeaders())
			.send(params)
			.end(function(response) {
				console.log("DELETE request", response);
			});
	};

	return instance;
};

if (typeof exports !== 'undefined') {
	if (typeof module !== 'undefined' && module.exports) {
		exports = module.exports = c42REST;
	}
	exports.c42REST = c42REST;
}

